package com.example.springbootcicdtestnew;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootCicdTestNewApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootCicdTestNewApplication.class, args);
    }

}
